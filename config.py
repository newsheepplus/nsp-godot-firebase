def can_build(env, platform):
	return platform=="android"

def configure(env):
	if (env['platform'] == 'android'):
		# env.android_add_maven_repository("url 'https://maven.fabric.io/public'")
		# env.android_add_maven_repository("url 'https://maven.google.com'")
		# env.android_add_maven_repository("url 'https://oss.sonatype.org/content/repositories/snapshots'")

		env.android_add_gradle_classpath("com.google.gms:google-services:4.1.0")
		env.android_add_gradle_plugin("com.google.gms.google-services")

		env.android_add_dependency("implementation('com.google.firebase:firebase-core:17.0.1')")
		env.android_add_dependency("implementation('com.google.firebase:firebase-analytics:17.0.1')")
		
		env.android_add_java_dir("android")
		env.android_add_default_config("applicationId 'com.newsheepplus.tynipogo'")
		env.disable_module()
