package org.godotengine.godot;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class GodotFirebase extends Godot.SingletonBase
{

    private Activity activity = null;
    private FirebaseAnalytics firebaseAnalytics;

    /**
     * Initialization
     */
    public void init() {
        // activity.runOnUiThread(new Runnable()
        // {
        //     @Override
        //     public void run()
        //     {
        firebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Log.i("godot", "Firebase: Init Firebase Analytics.");
        //     }
        // });
    }

    /**
     * New Methods
     */

    public void set_screen_name(final String s_name) {
        firebaseAnalytics.setCurrentScreen(activity, s_name, null);

        Log.i("GodotFireBase", "Setting current screen to: " + s_name);
    }

    public void send_achievement(final String id) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, id);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT, bundle);

        Log.i("GodotFireBase", "Sending:AchievementUnlocked: " + id);
    }

    public void send_group(final String groupID) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.GROUP_ID, groupID);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.JOIN_GROUP, bundle);

        Log.i("GodotFireBase", "Sending:JoinGroup: " + groupID);
    }

    public void send_level_up(final String character, final int level) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CHARACTER, character);
        bundle.putInt(FirebaseAnalytics.Param.LEVEL, level);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);

        Log.i("GodotFireBase", "Sending:Character:LevelUp: {" + character + "|" + level + "}");
    }

    public void send_score(final String character, final int level, final int score) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CHARACTER, character);
        bundle.putInt(FirebaseAnalytics.Param.LEVEL, level);
        bundle.putInt(FirebaseAnalytics.Param.SCORE, score);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.POST_SCORE, bundle);

        Log.i("GodotFireBase", "Sending:Level:Score: {" + character + "|" + level + "|" + score + "}");
    }

    public void send_content(final String content_type, final String item_id) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        Log.i("GodotFireBase", "Sending:Content:Item: {" + content_type + "|" + item_id + "}");
    }

    public void earn_currency(final String currency_name, final int value) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, currency_name);
        bundle.putInt(FirebaseAnalytics.Param.VALUE, value);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY, bundle);

        Log.i("GodotFireBase", "Sending:Currency:Earn: {" + currency_name + "|" + value + "}");
    }

    public void spend_currency(final String item_name, final String currency_name, final int value) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        bundle.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, currency_name);
        bundle.putInt(FirebaseAnalytics.Param.VALUE, value);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, bundle);

        Log.i("GodotFireBase", 
        "Sending:Currency:Spend: {" + item_name + "|" + currency_name + "|" + value + "}");
    }

    public void send_tutorial_begin() {
        Bundle bundle = new Bundle();
        //bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, id);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_BEGIN, bundle);

        Log.i("GodotFireBase", "Sending:Tutorial:Begin");
    }

    public void send_tutorial_complete() {
        Bundle bundle = new Bundle();
        //bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, id);

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_COMPLETE, bundle);
        Log.i("GodotFireBase", "Sending:Tutorial:Complete");
    }

    public void send_events(String eventName, Dictionary keyValues) {

        // Generate bundle out of keyValues
        Bundle bundle = new Bundle();
        putAllInDict(bundle, keyValues);
        // Dispatch event
        firebaseAnalytics.logEvent(eventName, bundle);

        Log.i("GodotFireBase", "Sending:App:Event:[" + eventName + "]:" + bundle.toString() + "");
    }

    public void send_custom(final String key, final String value) {
        Bundle bundle = new Bundle();
        bundle.putString(key, value);

        firebaseAnalytics.logEvent("appEvent", bundle);
        Log.i("GodotFireBase", "Sending:App:Event:" + bundle.toString());
    }

    private void putAllInDict(Bundle bundle, Dictionary keyValues) {
		String[] keys = keyValues.get_keys();
		for(int i=0; i < keys.length; i++) {
			String key = keys[i];
			putGodotValue(bundle, key, keyValues.get(key));
		}
	}

	private void putGodotValue(Bundle bundle, String key, Object value) {

		if (value instanceof Boolean) {
			bundle.putBoolean(key, (Boolean) value);

		} else if (value instanceof Integer) {
			bundle.putInt(key, (Integer) value);

		} else if (value instanceof Double) {
			bundle.putDouble(key, (Double) value);

		} else if (value instanceof String) {
			bundle.putString(key, (String) value);

		} else {

			if (value != null) {
				bundle.putString(key, value.toString());
			}

		}
	}

    // public void send_share() {
    //     // TODO

    //     //SHARE

    //     /**
    //     Bundle bundle = new Bundle();
    //     bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, id);
    //     firebaseAnalytics.logEvent(FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT, bundle);
    //     Log.i("GodotFireBase", "Sending Achievement: " + id);
    //     **/
    // }

    // public void share(final String content_type, final String item_id)
    // {
    //     activity.runOnUiThread(new Runnable()
    //     {
    //         @Override
    //         public void run()
    //         {
    //             Bundle bundle = new Bundle();
    //             bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
    //             bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);
    //             firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle);
    //             Log.i("godot", "Firebase: SHARE event.");
    //         }
    //     });
    // }

    /* Godot Methods
     * ********************************************************************** */

    /**
    * Singleton
    */
    static public Godot.SingletonBase initialize(Activity activity)
    {
        return new GodotFirebase(activity);
    }

    /**
     * Constructor
     * @param Activity Main activity
     */
    public GodotFirebase(Activity activity) {
        this.activity = activity;
        registerClass("Firebase", new String[] {
            "init",
            "set_screen_name","send_achievement","send_group",
            "send_level_up","send_score","send_content",
            "earn_currency","spend_currency","send_tutorial_begin",
            "send_tutorial_complete","send_events","send_custom"
        });
    }
}